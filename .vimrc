"    ______ _ _ _       _             _                    
"   |  ____(_) (_)     ( )           (_)                   
"   | |__   _| |_ _ __ |/ ___  __   ___ _ __ ___  _ __ ___ 
"   |  __| | | | | '_ \  / __| \ \ / / | '_ ` _ \| '__/ __|
"   | |    | | | | |_) | \__ \  \ V /| | | | | | | | | (__ 
"   |_|    |_|_|_| .__/  |___/   \_/ |_|_| |_| |_|_|  \___|
"                | |                                       
"                |_|                                       


"Basic options
color desert
set nocompatible
set number
"set rnu
set ruler
filetype off
syntax on
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab

"For Vundle
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe' 
Plugin 'scrooloose/nerdtree'
Plugin 'jaxbot/semantic-highlight.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'rust-lang/rust.vim'
Plugin 'vim-syntastic/syntastic'
"Plugin 'Shougo/deoplete.nvim'
"Plugin 'roxma/vim-hug-neovim-rpc'
call vundle#end()  "All Plugins must go before next line

"For pathogen
"execute pathogen#infect()

filetype plugin on
filetype plugin indent on

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"NerdTree
map <F4> :NERDTreeToggle<CR>:set number<CR>

"For R and LaTeX compiling
let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.rmd': 'markdown','.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
autocmd Filetype rmd map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>
autocmd Filetype tex map <F5> :!pdflatex<space>%<enter>

"Semantic Highlight colors
let g:semanticGUIColors = ['#b58900', '#cb4b16', '#dc322f', '#d33682', '#f0882f', '#268bd2', '#2aa198', '#859900']

"CTags shortcuts
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>

"For YouCompleteMe
let g:ycm_server_python_interpreter = 'python2'
let g:ycm_global_ycm_extra_conf = '~/.vim/ycm_extra_conf.py'
